import {createRouter, createWebHashHistory} from "vue-router";
import HomePage from "@/views/HomePage.vue";
import OpticsPage from "@/views/OpticsPage.vue";
import ClothingPage from "@/views/ClothingPage.vue";
import OutletPage from "@/views/OutletPage.vue";
import NakedHeroesPage from "@/views/NakedHeroesPage.vue";
import StoriesPage from "@/views/StoriesPage.vue";
import UserLogin from "@/views/UserLogin.vue";
import UserRegister from "@/views/UserRegister.vue";

export default createRouter({
    history: createWebHashHistory(),
    routes: [
        {path: '/', name: 'home', component: HomePage},
        {path: '/optics', name: 'optics', component: OpticsPage},
        {path: '/clothing', name: 'clothing', component: ClothingPage},
        {path: '/outlet', name: 'outlet', component: OutletPage},
        {path: '/naked-heroes', name: 'naked-heroes', component: NakedHeroesPage},
        {path: '/stories', name: 'stories', component: StoriesPage},
        {path: '/login', name: 'login', component: UserLogin},
        {path: '/register', name: 'register', component: UserRegister},
    ]
})
