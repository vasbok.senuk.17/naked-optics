import {createApp} from "vue";
import {createStore} from "vuex";

const store = createStore({
    state() {
        return {
            count: 0
        }
    },
    mutations: {
        increment(state) {
            state.count++
        }
    },
    actions: {

    }
})
const app = createApp({})
app.use(createStore)
app.use(store)